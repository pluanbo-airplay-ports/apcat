//#define BATCH

#include "QuGlobals.h"
#include "s3e.h"
#include "GuiAp.h"
#include "QuUtils.h"
#include "QuGameManager.h"


#include "Global.h"
#include "SuiAp.h"
#include "APMain.h"
#include "s3eKeyboard.h"
#include "QuMouse.h"

static const float F_TAP_DISTANCE_THRESHOLD = 0.015f;		//distance grace for a "tapping" gesture
static const float F_TAP_TIME_THRESHOLD = 200.0f; //ms grace for a "tapping" gesture
static const float F_SWIPE_THRESHOLD = 0.03f; //distance threshold for a "swiping" gesture
static const float MOUSE_MOVE_GRACE = 300; // ms backtracking on mouse gestures

GuiKeyType mapDirToGuiKeyP2(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GuiKeyType('j');
	else if(aInput & G_UP)
		return GuiKeyType('i');
	else if(aInput & G_RIGHT)
		return GuiKeyType('l');
	else if(aInput & G_DOWN)
		return GuiKeyType('k');
	
	quAssert(false);
	return GuiKeyType(0);
}

GuiKeyType mapDirToGuiKeyP1(GDirectionalInput aInput)
{
	if(aInput & G_LEFT)
		return GUI_LEFT;
	else if(aInput & G_UP)
		return GUI_UP;
	else if(aInput & G_RIGHT)
		return GUI_RIGHT;
	else if(aInput & G_DOWN)
		return GUI_DOWN;
	
	quAssert(false);
	return GuiKeyType(0);
}

void ApMapKeys(std::vector<GuiKeyType>& vApMapper)
{
	//this should take AP keys as index have GUI keys as value
    vApMapper.resize(400);
	vApMapper[s3eKeyLeft] = GUI_LEFT;
	vApMapper[s3eKeyRight] = GUI_RIGHT;
	vApMapper[s3eKeyUp] = GUI_UP;
	vApMapper[s3eKeyDown] = GUI_DOWN;
	vApMapper[s3eKeySpace] = GuiKeyType(32);
	vApMapper[s3eKeyBackSlash] = GuiKeyType(92);
	vApMapper[s3eKeyEnter] = GUI_RETURN;
}

unsigned GetTicks()
{
	return G_GAME_GLOBAL.getMillis();
}


class GenGuiManager : public QuBaseManager
{
	bool mInit;
	SP<GlobalController> pGl;
	SP<ApGraphicalInterface> pGraph;
	std::vector<GuiKeyType> vApMapper;

	QuAdvancedMultiTouchManager mTouches;
	std::map<int,GDirectionalInput> mLastDirection;	//note here unset entries default to 0 (G_DIRECTION_NEUTRAL) and we use this fact...
public:
	GenGuiManager():mInit(false)
	{
	}
	~GenGuiManager()
	{		
		
	}
	void guiInit()
	{
		srand( (unsigned)time( NULL ));
		try
		{
			ProgramInfo inf = GetProgramInfo();
			Rectangle sBound = Rectangle(Point(0, 0), inf.szScreenRez);
			//Rectangle sBound = Rectangle(Point(0, 0), Point(G_SCREEN_WIDTH,G_SCREEN_HEIGHT));
			pGraph = new ApGraphicalInterface(sBound.sz);
			SP< GraphicalInterface<Index> > pGr = new SimpleGraphicalInterface<ApImage*>(pGraph);
			SP<ApSoundInterface> pSound = new ApSoundInterface();
			SP< SoundInterface<Index> > pSndMng = new SimpleSoundInterface<ApSoundObject> (pSound);
			pGr->DrawRectangle(sBound, Color(0,0,0), true);
			if(inf.bMouseCapture)
			{
			}

			ProgramEngine pe(new EmptyEvent(), pGr, pSndMng, new IoWriter(),inf.szScreenRez);
			pGl = GetGlobalController(pe);
			std::cout << "global controller constructed" << std::endl;
			ApMapKeys(vApMapper);
			Point pMousePos = Point(0,0);

		}
		catch(MyException& me)
		{
			std::cout << "exceptin in main setup: " << me.GetDescription(true) << "\n";
		}
		catch(...)
		{
			std::cout << "Unknown error!\n";
		}
	}
	virtual void initialize()
	{
		glClearColor(0,0,0,1);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		guiInit();
		mInit = true;
	}
	virtual void update()
	{
		//pGraph->RefreshAll();
		runGui();
		pGraph->draw();
		
	}
	virtual void keyDown(int key){ pGl->KeyDown(vApMapper[key]); }
	virtual void keyUp(int key){ pGl->KeyUp(vApMapper[key]); }
	virtual void singleDown(int button, QuScreenCoord crd){ sendButton(GuiKeyType(' ')); }
	virtual void singleUp(int button, QuScreenCoord crd){}
	virtual void singleMotion(QuScreenCoord crd){}

	void sendButton(GuiKeyType key)
	{
		pGl->KeyDown(key);
		pGl->KeyUp(key);
	}
	void sendClick(QuVector2<int> p)
	{
		//std::cout << p.x << " " << p.y << std::endl;
		pGl->MouseClick(Gui::Point(p.x,p.y));
	}
	void runGui()
	{
		try{
  			pGl->Update();
		}
		catch(...){
			std::cout << "exception" << std::endl;
			s3eDeviceExit();
		}	
	}
};

int main()
{
	
	QuStupidPointer<QuGlobalSettings> s = new QuGlobalTypeSettings<QuApRawSoundManager<22050> ,QuImageManager,ApFileManager>();
	s->mRefresh = false;
	INITIALIZE_GAME<GenGuiManager>(s);
	//return APMain(new GenGuiManager());
}

